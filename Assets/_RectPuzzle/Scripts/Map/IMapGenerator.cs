﻿using UnityEngine;

namespace _RectPuzzle.Map
{
    public interface IMapGenerator
    {
        void GenerateMap(int width, int height, int minArea, int maxArea);
        void Clear();
        Bounds GetMapBounds();
    }
}