﻿using System;
using Random = UnityEngine.Random;

namespace _RectPuzzle.Map
{
    public readonly struct Rectangle
    {
        public int Area => (EndX - StartX) * (EndY - StartY);

        public int StartX { get; }
        public int EndX { get; }
        public int StartY { get; }
        public int EndY { get; }
        
        private int Width => EndX - StartX;
        private int Height => EndY - StartY;

        #region Constructors

        public Rectangle(int width, int height)
        {
            StartX = 0;
            StartY = 0;
            EndX = width;
            EndY = height;
        }
        
        private Rectangle(int startX, int endX, int startY, int endY)
        {
            StartX = startX;
            EndX = endX;
            StartY = startY;
            EndY = endY;
        }

        #endregion
        
        public bool IsCanDivide(int minArea)
        {
            return Area >= minArea * 2;
        }

        public int GetMaxPossibleCount(int minArea)
        {
            return Width * Height / minArea;
        }

        public (Rectangle, Rectangle) Divide(int minArea)
        {
            if (Area == minArea * 2)
                return DivideInHalf();
            
            if (Area > minArea * 2)
                return DivideByRandom(minArea);
            
            throw new Exception("Current rectangle can`t be divided! It`s too small!");
        }

        private (Rectangle, Rectangle) DivideInHalf()
        {
            bool possibleByX = Width % 2 == 0;
            bool possibleByY = Height % 2 == 0;

            if (possibleByX && possibleByY)
                return Random.value > .5f ? DivideInHalfByX() : DivideInHalfByY();

            if (possibleByX)
                return DivideInHalfByX();

            if (possibleByY)
                return DivideInHalfByY();
            
            throw new Exception("Can`t divide rectangle for some reason!");
        }

        private (Rectangle, Rectangle) DivideByRandom(int minArea)
        {
            bool possibleByX = Math.Floor(Width / 2d) * Height >= minArea 
                               && Math.Ceiling(Width / 2d) * Height >= minArea;
            bool possibleByY = Math.Floor(Height / 2d) * Width >= minArea 
                               && Math.Ceiling(Height / 2d) * Width >= minArea;

            if (possibleByX && possibleByY)
                return Random.value > .5f ? DivideRandomByX(minArea) : DivideRandomByY(minArea);
            
            if(possibleByX)
                return DivideRandomByX(minArea);
            
            if(possibleByY)
                return DivideRandomByY(minArea);
            
            throw new Exception("Can`t divide rectangle for some reason!");
        }

        private (Rectangle, Rectangle) DivideInHalfByY()
        {
            int midY = StartY + Height / 2;
            var r1 = new Rectangle(StartX, EndX, StartY, midY);
            var r2 = new Rectangle(StartX, EndX, midY, EndY);
            return (r1, r2);
        }

        private (Rectangle, Rectangle) DivideInHalfByX()
        {
            int midX = StartX + Width / 2;
            var r1 = new Rectangle(StartX, midX, StartY, EndY);
            var r2 = new Rectangle(midX, EndX, StartY, EndY);
            return (r1, r2);
        }

        private (Rectangle, Rectangle) DivideRandomByY(int minArea)
        {
            (int min, int max) = GetRangeForRandomDivideByY(minArea);
            int midY = StartY + Random.Range(min, max + 1);
            var r1 = new Rectangle(StartX, EndX, StartY, midY);
            var r2 = new Rectangle(StartX, EndX, midY, EndY);
            return (r1, r2);
        }

        private (Rectangle, Rectangle) DivideRandomByX(int minArea)
        {
            (int min, int max) = GetRangeForRandomDivideByX(minArea);
            int midX = StartX + Random.Range(min, max + 1);
            var r1 = new Rectangle(StartX, midX, StartY, EndY);
            var r2 = new Rectangle(midX, EndX, StartY, EndY);
            return (r1, r2);
        }
        
        private (int, int) GetRangeForRandomDivideByX(int minArea)
        {
            var min = 0;

            for (var i = 0; i < Width; i++)
            {
                if ((i + 1) * Height < minArea) 
                    continue;
                
                min = i + 1;
                
                break;
            }

            return (min, Width - min);
        }
        
        private (int, int) GetRangeForRandomDivideByY(int minArea)
        {
            var min = 0;

            for (var i = 0; i < Height; i++)
            {
                if ((i + 1) * Width < minArea) 
                    continue;
                
                min = i + 1;
                
                break;
            }

            return (min, Height - min);
        }
    }
}