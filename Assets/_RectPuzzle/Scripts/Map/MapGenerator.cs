﻿using System.Collections.Generic;
using _RectPuzzle.Extensions;
using _RectPuzzle.Tiles;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _RectPuzzle.Map
{
    public class MapGenerator : MonoBehaviour, IMapGenerator
    {
        [Title("Tiles settings")]
        [SerializeField] private TilesStack _tileStackPrefab;
        [SerializeField, ChildGameObjectsOnly] private Transform _tilesParent;
        [SerializeField] private List<Color> _colors = new List<Color>();
        [Title("Cells settings")]
        [SerializeField, AssetsOnly] private SpriteRenderer _cellPrefab;
        [SerializeField, ChildGameObjectsOnly] private Transform _cellsParent;
        [SerializeField] private float _cellSize = 1f;
        [SerializeField] private float _cellOffset = .1f;

        private SpriteRenderer[][] _cellsMap;
        private readonly List<TilesStack> _tilesStacks = new List<TilesStack>();
        private int _minArea;
        private int _maxArea;

        public void GenerateMap(int width, int height, int minArea, int maxArea)
        {
            _minArea = minArea;
            _maxArea = maxArea;
            GenerateField(width, height);
            List<Rectangle> rectangles = GenerateRectangles(width, height);
            SpawnTiles(width, rectangles);
        }

        public void Clear()
        {
            foreach (TilesStack tilesStack in _tilesStacks)
                Destroy(tilesStack.gameObject);
            
            _tilesStacks.Clear();
            
            foreach (SpriteRenderer[] cells in _cellsMap)
            {
                foreach (SpriteRenderer cell in cells)
                    Destroy(cell.gameObject);
            }
            
            _cellsMap = null;
        }

        public Bounds GetMapBounds()
        {
            var bounds = new Bounds(transform.position, Vector3.zero);
            
            foreach (SpriteRenderer[] cellsRow in _cellsMap)
                foreach (SpriteRenderer cellSprite in cellsRow)
                    bounds.Encapsulate(cellSprite.bounds);

            return bounds;
        }

        private List<Rectangle> GenerateRectangles(int width, int height)
        {
            var mainRectangle = new Rectangle(width, height);
            var rectangles = new List<Rectangle> { mainRectangle };

            while (TryGetRectangleToDivide(rectangles, out Rectangle rectToDivide))
            {
                rectangles.Remove(rectToDivide);
                (Rectangle, Rectangle) dividedRects = rectToDivide.Divide(_minArea);
                rectangles.Add(dividedRects.Item1);
                rectangles.Add(dividedRects.Item2);
            }

            return rectangles;
        }

        private bool TryGetRectangleToDivide(IEnumerable<Rectangle> rectangles, out Rectangle rectangleToDivide)
        {
            rectangleToDivide = new Rectangle();
            
            foreach (Rectangle rectangle in rectangles)
            {
                if (!rectangle.IsCanDivide(_minArea) || rectangle.Area <= _maxArea) 
                    continue;
                
                rectangleToDivide = rectangle;
                return true;
            }

            return false;
        }

        private void SpawnTiles(int width, List<Rectangle> rectangles)
        {
            float startX = (width * _cellSize + (width - 1) * _cellOffset) / -2f + _cellSize / 2f;
            var colorsListCopy = new List<Color>(_colors);
            
            foreach (Rectangle rect in rectangles)
            {
                int posX = Random.value > .5f ? rect.StartX : rect.EndX - 1;
                int posZ = Random.value > .5f ? rect.StartY : rect.EndY - 1;
                Color color = colorsListCopy.GetRandomAndRemove();
                TilesStack tilesStack = Instantiate(_tileStackPrefab, _tilesParent);
                tilesStack.transform.localPosition = new Vector3( startX + posX * _cellSize + posX * _cellOffset,
                    0,
                    posZ * _cellSize + posZ * _cellOffset);
                tilesStack.Initialize(rect.Area, color);
                _tilesStacks.Add(tilesStack);
            }
        }

        private void GenerateField(int width, int height)
        {
            _cellsMap = new SpriteRenderer[width][];
            
            for (var i = 0; i < width; i++)
                _cellsMap[i] = new SpriteRenderer[height];
            
            float startX = (width * _cellSize + (width - 1) * _cellOffset) / -2f + _cellSize / 2f;

            for (var x = 0; x < width; x++)
            {
                for (var z = 0; z < height; z++)
                {
                    SpriteRenderer cell = Instantiate(_cellPrefab, _cellsParent);
                    _cellsMap[x][z] = cell;
                    cell.transform.localPosition = new Vector3( startX + x * _cellSize + x * _cellOffset,
                                                                0,
                                                                z * _cellSize + z * _cellOffset);
                }
            }
        }
    }
}