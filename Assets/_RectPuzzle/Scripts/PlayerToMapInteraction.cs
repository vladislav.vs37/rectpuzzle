﻿using System;
using _RectPuzzle.Drawing;
using _RectPuzzle.Map;
using _RectPuzzle.Tiles;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using Zenject;

namespace _RectPuzzle
{
    public class PlayerToMapInteraction : MonoBehaviour
    {
        private const int MaxRaycastDistance = 30;
        private const string TileLayerName = "Tile";
        private const string GroundLayerName = "Ground";

        private ISelectionRectDrawer _selectionRectDrawer;
        private IMapGenerator _mapGenerator;
        private Camera _mainCamera;
        private int _tileLayerMask;
        private bool _drawing;
        private TilesStack _selectedStack;
        private int _groundLayerMask;
        private Bounds _mapBounds;
        private bool _pause;

        [Inject]
        public void Construct(ISelectionRectDrawer selectionRectDrawer, IMapGenerator mapGenerator)
        {
            _selectionRectDrawer = selectionRectDrawer;
            _mapGenerator = mapGenerator;
        }

        private void Awake()
        {
            _mainCamera = Camera.main;
            _tileLayerMask = LayerMask.GetMask(TileLayerName);
            _groundLayerMask = LayerMask.GetMask(GroundLayerName);
        }

        private void Update()
        {
            if(_pause)
                return;
            
            if (Input.GetMouseButtonDown(0))
            {
                if (!TryGetTilesStack(out TilesStack tilesStack) || tilesStack.Expanded) 
                    return;

                StartDrawSelectionRect(tilesStack);
            }
            else if (_drawing && Input.GetMouseButtonUp(0))
            {
                StopDrawSelectionRect();
            } else if (!_drawing && Input.GetMouseButtonUp(0))
            {
                if (!TryGetTilesStack(out TilesStack tilesStack) || !tilesStack.Expanded)
                    return;
                        
                tilesStack.Fold();
            }
            else if (_drawing && Input.GetMouseButton(0))
            {
                if(!TryGetDrawPosition(out Vector3 drawPosition))
                    return;
                
                _selectionRectDrawer.UpdateDrawPosition(drawPosition);

                if(IsOverlapsOtherTiles())
                    StopDrawingWithError();
            }
        }

        private void StopDrawingWithError()
        {
            _pause = true;
            _drawing = false;
            _selectionRectDrawer.CloseWithError(OnCloseSelectionRect);
        }

        private void OnDrawGizmos()
        {
            if(_selectionRectDrawer == null)
                return;
            
            Vector3 center = _selectionRectDrawer.Bounds.center;
            Gizmos.DrawCube(center, _selectionRectDrawer.Bounds.size);
        }

        private void OnCloseSelectionRect()
        {
            _pause = false;
            _selectedStack.ShowOutline(false);
        }
        
        private bool IsOverlapsOtherTiles()
        {
            Vector3 center = _selectionRectDrawer.Bounds.center;
            Vector3 halfSize = _selectionRectDrawer.Bounds.size / 2;
            Collider[] colliders = Physics.OverlapBox(center, halfSize, quaternion.identity, _tileLayerMask);
                
            foreach (Collider collider in colliders)
            {
                var tile = collider.GetComponent<Tile>();
                    
                if(tile != null && !_selectedStack.Contains(tile))
                    return true;
            }

            return false;
        }

        private void StartDrawSelectionRect(TilesStack tilesStack)
        {
            _drawing = true;
            _mapBounds = _mapGenerator.GetMapBounds();
            _selectedStack = tilesStack;
            _selectedStack.ShowOutline(true);
            _selectionRectDrawer.Color = _selectedStack.Color;
            _selectionRectDrawer.StartDraw(_selectedStack.transform.position);
        }

        private void StopDrawSelectionRect()
        {
            if (Mathf.Abs(_selectionRectDrawer.RectSize.y * _selectionRectDrawer.RectSize.x) == _selectedStack.Count)
            {
                _drawing = false;
                _selectedStack.ShowOutline(false);
                _selectionRectDrawer.StopDraw();
                _selectedStack.Expand(_selectionRectDrawer.RectSize);
            }
            else
            {
                StopDrawingWithError();
            }
        }

        private bool TryGetDrawPosition(out Vector3 drawPosition)
        {
            drawPosition = Vector3.zero;
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            
            if (!Physics.Raycast(ray, out RaycastHit hit, MaxRaycastDistance, _groundLayerMask))
                return false;

            drawPosition = new Vector3(Mathf.Clamp(hit.point.x, _mapBounds.min.x, _mapBounds.max.x),
                hit.point.y,
                Mathf.Clamp(hit.point.z, _mapBounds.min.z, _mapBounds.max.z));

            return true;
        }

        private bool TryGetTilesStack(out TilesStack tilesStack)
        {
            tilesStack = null;
            Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

            if (!Physics.Raycast(ray, out RaycastHit hit, MaxRaycastDistance, _tileLayerMask))
                return false;

            tilesStack = hit.collider.GetComponentInParent<TilesStack>();

            return tilesStack != null;
        }
    }
}