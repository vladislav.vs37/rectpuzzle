﻿using System.Collections.Generic;
using UnityEngine;

namespace _RectPuzzle.Extensions
{
    public static class ListExtension
    {
        public static T GetRandomAndRemove<T>(this List<T> list)
        {
            int index = Random.Range(0, list.Count);
            T item = list[index];
            list.RemoveAt(index);
            
            return item;
        }
    }
}