﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace _RectPuzzle.Progression
{
    [CreateAssetMenu(fileName = "New level", menuName = "RectPuzzle/Level", order = 0)]
    public class Level : ScriptableObject
    {
        [SerializeField, MinValue(0)] private int _width;
        [SerializeField, MinValue(0)] private int _height;
        [SerializeField, MinValue(0)] private int _minArea;
        [SerializeField, MinValue(0)] private int _maxArea;

        public int Width => _width;

        public int Height => _height;

        public int MinArea => _minArea;

        public int MaxArea => _maxArea;
    }
}