﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _RectPuzzle.Drawing
{
    public class SelectionRectDrawer : MonoBehaviour, ISelectionRectDrawer
    {
        [SerializeField] private float _cellSize = 1f;
        [SerializeField] private float _cellOffset = 0.1f;
        [SerializeField] private SpriteRenderer _spriteRenderer;

        public Color Color
        {
            set => _spriteRenderer.color = value;
            get => _spriteRenderer.color;
        }
        
        public Bounds Bounds => _spriteRenderer.bounds;
        public Vector2Int RectSize { get; private set; }

        private Vector2 _rectSize;
        private Vector3 _initialPosition;

        private void Awake()
        {
            _spriteRenderer.gameObject.SetActive(false);
        }

        public void StartDraw(Vector3 initialPos)
        {
            _spriteRenderer.gameObject.SetActive(true);
            _spriteRenderer.size = Vector2.zero;
            _rectSize = Vector2.zero;
            _initialPosition = initialPos - new Vector3(_cellSize / 2,0,_cellSize / 2);
            transform.position = _initialPosition;
        }

        public void UpdateDrawPosition(Vector3 drawPos)
        {
            CalculateSelectionRectSize(drawPos);
            DrawSelectionRectangle();
        }

        public void StopDraw()
        {
            _spriteRenderer.size = Vector2.zero;
            _rectSize = Vector2.zero;
            _spriteRenderer.gameObject.SetActive(false);
        }

        public void CloseWithError(Action onClosed)
        {
            _spriteRenderer.color = Color.red;
            _rectSize = Vector2.zero;
            _spriteRenderer.DOFade(0, .5f).OnComplete(onClosed.Invoke);
        }

        private void DrawSelectionRectangle()
        {
            transform.position = new Vector3(
                _rectSize.x > 0 ? _initialPosition.x : _initialPosition.x + _cellSize,
                _initialPosition.y,
                _rectSize.y > 0 ? _initialPosition.z : _initialPosition.z + _cellSize);

            _spriteRenderer.size = _rectSize;
        }

        private void CalculateSelectionRectSize(Vector3 pointerPosition)
        {
            Vector2 size = Vector2.one * _cellSize;
            Vector3 localPointerPosition = transform.InverseTransformPoint(pointerPosition);
            
            int sizeX = RoundAxisValue(localPointerPosition.x);
            size.x = sizeX + (sizeX - (sizeX > 0 ? 1 : -1)) * _cellOffset;
            
            int sizeY = RoundAxisValue(localPointerPosition.z);
            size.y = sizeY + (sizeY - (sizeY > 0 ? 1 : -1)) * _cellOffset;

            RectSize = new Vector2Int(sizeX, sizeY);
            _rectSize = size;
        }

        private int RoundAxisValue(float value)
        {
            float fullSize = _cellSize + _cellOffset;
            
            return value > 0 ? Mathf.CeilToInt(value / fullSize) : Mathf.FloorToInt(value / fullSize);
        }
    }
}