﻿using System;
using UnityEngine;

namespace _RectPuzzle.Drawing
{
    public interface ISelectionRectDrawer
    {
        Color Color { set; get; }
        Bounds Bounds { get; }
        Vector2Int RectSize { get; }
        void StartDraw(Vector3 initialPos);
        void UpdateDrawPosition(Vector3 drawPos);
        void StopDraw();
        void CloseWithError(Action onClosed);
    }
}