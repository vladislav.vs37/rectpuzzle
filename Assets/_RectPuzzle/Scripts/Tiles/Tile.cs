﻿using UnityEngine;

namespace _RectPuzzle.Tiles
{
    [RequireComponent(typeof(BoxCollider))]
    public class Tile : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _meshRenderer;

        public void SetMaterial(Material material)
        {
            _meshRenderer.sharedMaterial = material;
        }
    }
}