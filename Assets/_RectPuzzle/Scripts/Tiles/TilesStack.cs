﻿using System.Collections.Generic;
using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace _RectPuzzle.Tiles
{
    [SelectionBase]
    public class TilesStack : MonoBehaviour
    {
        [SerializeField, AssetsOnly] private Tile _tilePrefab;
        [SerializeField, AssetsOnly] private Material _tileMaterial;
        [SerializeField, MinValue(0)] private float _tileHeight;
        [SerializeField, MinValue(0)] private float _outlineWidth;
        [SerializeField, ChildGameObjectsOnly] private TextMeshPro _areaText;

        public Color Color { get; private set; }
        public int Count { get; private set; }
        public bool Expanded { get; private set; }

        private readonly List<Tile> _tiles = new List<Tile>();
        private Material _materialCopy;
        private Outline _outline;
        private Vector2Int _expandSize;
        
        private const float _tileMoveDuration = .2f;
        
        public void Initialize(int count, Color color)
        {
            Count = count;
            Color = color;
            ClearPreviousData();
            SetCountText(count);
            CreateMaterialCopyWithNewColor();
            SpawnTiles(count);
            CreateOutline();
        }

        public void ShowOutline(bool show)
        {
            _outline.enabled = show;
        }
        
        public bool Contains(Tile tile)
        {
            return _tiles.Contains(tile);
        }

        public void Expand(Vector2Int rectSize)
        {
            Expanded = true;
            _areaText.gameObject.SetActive(false);
            _expandSize = rectSize;
            Sequence sequence = DOTween.Sequence();

            var tilesIndex = 0;
            float timePosition = 0;
            var skipOnce = true;
            
            for (int i = 0; i < Mathf.Abs(rectSize.x); i++)
            {
                for (int j = 0; j < Mathf.Abs(rectSize.y); j++)
                {
                    if (skipOnce == false)
                    {
                        Vector3 position = transform.position;
                        position.x += i * 1.1f * (rectSize.x > 0 ? 1 : -1);
                        position.z += j * 1.1f * (rectSize.y > 0 ? 1 : -1);
                    
                        for (int k = tilesIndex; k < _tiles.Count; k++)
                        {
                            if(k > tilesIndex)
                                position.y = _tiles[k].transform.position.y;
                        
                            sequence.Insert(timePosition, _tiles[k].transform.DOJump(position, 1, 1, _tileMoveDuration).SetEase(Ease.Linear));
                        }
                    } 
                    
                    tilesIndex++;
                    timePosition += _tileMoveDuration;

                    if (skipOnce)
                        skipOnce = false;
                }
            }
            
            sequence.OnComplete(OnExpandComplete);
        }

        public void Fold()
        {
            Expanded = false;
            _areaText.gameObject.SetActive(false);
            Sequence sequence = DOTween.Sequence();
            float timePosition = 0;

            for (var i = 0; i < _tiles.Count; i++)
            {
                _tiles[i].SetMaterial(_materialCopy);
                sequence.Insert(timePosition, _tiles[i].transform.DOLocalMove(Vector3.up * (_tileHeight * i), _tileMoveDuration));
                timePosition += .1f;
            }
            
            sequence.OnComplete(OnFoldComplete);
        }

        private void OnFoldComplete()
        {
            ShowOutline(false);
            _areaText.gameObject.SetActive(true);
            _areaText.transform.localPosition = Vector3.up * (_tileHeight * _tiles.Count + _tileHeight / 2 + .1f);
        }

        private void OnExpandComplete()
        {
            _areaText.gameObject.SetActive(true);
            _areaText.transform.localPosition = Vector3.up * (_tileHeight + _tileHeight / 2 + .1f);
        }

        private void CreateOutline()
        {
            Color = AddSaturation(Color);
            _outline = gameObject.AddComponent<Outline>();
            _outline.OutlineColor = Color;
            _outline.OutlineMode = Outline.Mode.OutlineVisible;
            _outline.OutlineWidth = _outlineWidth;
            _outline.enabled = false;
        }

        private static Color AddSaturation(Color color)
        {
            Color.RGBToHSV(color, out float h, out float s, out float v);
            s = Mathf.Clamp01(s + .2f);
            
            return Color.HSVToRGB(h, s, v);
        }

        private void CreateMaterialCopyWithNewColor()
        {
            _materialCopy = new Material(_tileMaterial)
            {
                color = Color
            };
        }

        private void SetCountText(int count)
        {
            _areaText.text = count.ToString();
            _areaText.transform.localPosition = Vector3.up * (_tileHeight * count + _tileHeight / 2 + .1f);
        }

        private void SpawnTiles(int count)
        {
            for (var i = 0; i < count; i++)
            {
                Tile tile = Instantiate(_tilePrefab, transform);
                tile.transform.localPosition = Vector3.up * (_tileHeight * i);
                tile.SetMaterial(_materialCopy);
                _tiles.Add(tile);
            }
        }

        private void ClearPreviousData()
        {
            foreach (Tile tile in _tiles)
                Destroy(tile.gameObject);
            
            _tiles.Clear();

            if (_outline)
                Destroy(_outline);
        }
    }
}