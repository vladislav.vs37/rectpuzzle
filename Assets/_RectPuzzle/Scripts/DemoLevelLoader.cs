﻿using _RectPuzzle.Map;
using _RectPuzzle.Progression;
using UnityEngine;
using Zenject;

namespace _RectPuzzle
{
    public class DemoLevelLoader : MonoBehaviour
    {
        [SerializeField] private Level _level;
        private IMapGenerator _mapGenerator;

        [Inject]
        public void Construct(IMapGenerator mapGenerator)
        {
            _mapGenerator = mapGenerator;
        }

        private void Start()
        {
            _mapGenerator.GenerateMap(_level.Width, _level.Height, _level.MinArea, _level.MaxArea);
        }
    }
}