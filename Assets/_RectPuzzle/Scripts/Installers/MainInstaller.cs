﻿using _RectPuzzle.Drawing;
using _RectPuzzle.Map;
using UnityEngine;
using Zenject;

namespace _RectPuzzle.Installers
{
    public class MainInstaller : MonoInstaller
    {
        [SerializeField] private MapGenerator _mapGenerator;
        [SerializeField] private SelectionRectDrawer _selectionRectDrawer;
        
        public override void InstallBindings()
        {
            Container.Bind<IMapGenerator>().FromInstance(_mapGenerator).AsSingle();
            Container.Bind<ISelectionRectDrawer>().FromInstance(_selectionRectDrawer).AsSingle();
        }
    }
}